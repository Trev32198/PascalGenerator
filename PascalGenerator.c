#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
PascalGenerator - A simple program for generating rows of Pascal's triangle
Copyright (C) 2017  Trevor Dumas

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

int main(int argc, char *argv[])
{
	// Determine if the user passed an argument requesting help (-h or --help)
	unsigned char helpNeeded = 0;
	for (int i = 0; i < argc; i++)
	{
		if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
		{
			helpNeeded = 1;
			break;
		}
	}
	// If a help argument was passed, print usage and exit with code 0
	if (helpNeeded)
	{
		printf("USAGE: PascalGenerator [first row number] [last row number]\n");
		return 0;
	}
	// If the user enters an invalid number of arguments, print an error message and
	// the program's usage to stderr and exit with code 1 
	if (argc != 3)
	{
		fprintf(stderr, "Error: Incorrect number of arguments given (expected 2).\n");
		fprintf(stderr, "USAGE: PascalGenerator [first row number] [last row number]\n");
		return 1;
	}
	// Create variables to hold the arguments after they are converted to integers
	int firstRowNumber = 0;
	int lastRowNumber = 0;
	// Try to convert the arguments to integers
	// If one or both conversions fail, print an error message and exit with code 2
	if (sscanf(argv[1], "%d", &firstRowNumber) != 1 || sscanf(argv[2], "%d", &lastRowNumber) != 1)
	{
		fprintf(stderr, "Error: At least one argument could not be converted to an integer.\n");
		return 2;
	}
	// Treat negative arguments as 0
	if (firstRowNumber < 0)
	{
		firstRowNumber = 0;
	}
	if (lastRowNumber < 0)
	{
		lastRowNumber = 0;
	}
	// If firstRowNumber is greater than lastRowNumber, switch the values of the two variables
	if (firstRowNumber > lastRowNumber)
	{
		int temp = firstRowNumber;
		firstRowNumber = lastRowNumber;
		lastRowNumber = temp;
	}
	// Create an array of two pointers to rows of integers in Pascal's triangle
	// (only two rows will be in memory at a time)
	long long unsigned int *rows[2] = {NULL, NULL};
	// Generate and print the needed rows of Pascal's triangle
	for (int i = 0; i <= lastRowNumber; i++)
	{
		// Free the second to last row processed
		free(rows[0]);
		// Replace the second to last row's freed pointer with a pointer to the last row processed
		rows[0] = rows[1];
		// Allocate enough memory for the next row to be processed
		rows[1] = malloc(sizeof(long long unsigned int) * (i + 1));
		// Check if malloc failed
		if (rows[1] == NULL)
		{
			fprintf(stderr, "Error: Could not allocate enough memory.\n");
			return 3;
		}
		// Set the ends of the row to 1
		rows[1][0] = 1;
		rows[1][i] = 1;
		// Compute the rest of the row's numbers
		for (int k = 1; k < i; k++)
		{	
			// Compute each number (other than the first and last) by adding the number at the
			// same index in the previous row with the number at the same index minus one in
			// the previous row
			rows[1][k] = rows[0][k] + rows[0][k - 1];
		}
		// If the newly computed row's number is greater than or equal to firstRowNumber, print it
		if (i >= firstRowNumber)
		{
			for (int k = 0; k <= i; k++)
			{
				printf("%llu ", rows[1][k]);
			}
			printf("\n");
		}
	}
	return 0;
}
