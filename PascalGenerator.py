#!/usr/bin/env python
from sys import argv

"""
PascalGenerator - A simple program for generating rows of Pascal's triangle
Copyright (C) 2017  Trevor Dumas

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

def main():
    # If the number of arguments passed is incorrect or if help is requested, print usage and exit
    if (len(argv) != 3 or "-h" in argv or "--help" in argv):
        print("USAGE: PascalGenerator.py [first row number] [last row number]")
        exit(1)
    try:
        # Parse arguments
        firstRowNumber = int(argv[1])
        lastRowNumber = int(argv[2])
    except:
        print("Could not parse arguments as integers.")
        exit(1)
    # Create an array to hold two rows of integers at a time for processing
    rows = [[], []]
    for i in range(lastRowNumber + 1):
        # Replace the oldest row in memory with the latest row processed
        rows[0] = rows[1]
        # Create a new row of the correct length (populated only by 1s)
        rows[1] = [1] * (i + 1)
        # Compute the row
        for k in range(1, i):
            # Compute each number (other than the first and last) by adding the number at the
	    # same index in the previous row with the number at the same index minus one in
	    # the previous row
            rows[1][k] = rows[0][k] + rows[0][k - 1]
        # If the row number is within the range to be printed, print it
        if i >= firstRowNumber:
            # Print each number on a single line (call print without no newline)
            for number in rows[1]:
                print(number, end=" ")
            # Advance to next line
            print()
        


if __name__ == "__main__":
    main()
